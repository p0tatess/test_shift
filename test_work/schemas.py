from pydantic import BaseModel
from datetime import datetime



class UserBase(BaseModel):
    username:str
    email:str


class UserCreate(UserBase):
    password:str


class User(UserBase):
    id:int
    random_key:str
    time_created:datetime
    raise_zp_info:int

    class Config:
        orm_mode=True










