import random
from faker import Faker
from sqlalchemy import Column, Integer, String, TIMESTAMP, ForeignKey,DATE
from datetime import datetime
from sqlalchemy.orm import relationship
from .database import Base
from secrets import token_hex

fake = Faker()


class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True,index=True)
    username=Column(String,unique=True)
    email = Column(String, unique=True)
    hashed_password= Column(String)
    token = Column(String,default=token_hex(8))
    time_created = Column(TIMESTAMP,default=datetime.now)
    raise_zp_info = relationship('InfoUser',uselist=False,back_populates='user')


class InfoUser(Base):
    __tablename__ = 'infousers'

    id = Column(Integer,primary_key=True,index=True)
    wage = Column(Integer,default=random.randint(5000,100000))
    raising = Column(DATE,default=fake.date_between(start_date='today',end_date='+2y'))
    person_id = Column(Integer, ForeignKey("users.id"))
    user= relationship('User',back_populates='raise_zp_info')



