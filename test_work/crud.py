from sqlalchemy.orm import Session
from .models import User,InfoUser
from fastapi.responses import HTMLResponse
import jwt
import datetime


SECRET_KEY ='mysecretkey'
algorithm='HS256'

#Создание уникального токена каждые 30 минут
def create_acess_token(username:str):
    issued_at=datetime.datetime.utcnow()
    expires_at=issued_at + datetime.timedelta(minutes=30)
    payload={
        'sub':username,
        'iat':issued_at,
        'exp':expires_at,
    }
    token=jwt.encode(payload,SECRET_KEY,algorithm=algorithm)
    return token

#Декодирование и проверка валидности токена
def decode_acess_token(token:str):
    try:
        payload=jwt.decode(token,SECRET_KEY,algorithms=[algorithm])
        return payload
    except jwt.ExpiredSignatureError:
        print('Токен исстёк')
        return None
    except jwt.InvalidTokenError:
        print('Невалидный токен')
        return None


#Проверка на уникальность email пользователя
def get_user_by_email(db: Session, email: str):
    return db.query(User).filter(User.email == email).first()

#Создание пользователя,имитация хеширования пароля с занесением информации в бд
def create_user(db: Session, username:str,email:str,password:str):
    fake_hashed_password = password + "fakehashed"
    db_user = User(username=username,email=email, hashed_password=fake_hashed_password)
    db_info = InfoUser(user=db_user)
    db.add(db_user)
    db.add(db_info)
    db.commit()
    db.refresh(db_user)
    db.refresh(db_info)
    return HTMLResponse("""
    <h1>Запись успешно создана</h1>
    <a href='get_token'>Узнать свой токен</a>
    """)


#Проверка существования пользователя, проверка валидности токена и вывод его пользователю
def get_secret_token(username:str,password:str,db:Session):
    fake_hashed_pasword=password + 'fakehashed'
    info =db.query(User).filter(User.username==username,User.hashed_password==fake_hashed_pasword).scalar()

    if info!=None:
        key= decode_acess_token(token=info.token)
        if key==None:
            secret= create_acess_token(username=username)
            info.token= secret
            db.add(info)
            db.commit()
            db.refresh(info)
            return HTMLResponse(f"""
               <h3>Ваш новый токен: {info.token}</h3>
               <a href='info_token'>Информация по токену</a> 
               """)
        else:
            return HTMLResponse(f"""
               <h3>Ваш токен: {info.token}</h3>
               <a href='info_token'>Информация по токену</a> 
               """)

    else:
        return HTMLResponse("""
        <h3>Данные оп вашему запросу не найдены, возможно вы допустили ошибку в написании</h3>
        <a href='get_token'>Попробовать ещё раз</a>
        """)



#Проверка валидности токена и выдача информации о зарплате и дате повышения для пользователя
def token_info(token:str,db:Session):
    info = db.query(User).filter(User.token==token).scalar()

    if info:
        key = decode_acess_token(token=info.token)
        if key==None:
            return HTMLResponse("""
            <h3>Введённый токен невалиден либо исстёк срок его использования</h3>
            <a href='get_token'>Узнать свой токен</a> 
            """)

        else:
            zp_raise = info.raise_zp_info
    else:
        return HTMLResponse("""
        <h3>Вы ввели неверный токен</h3>
        <a href='info_token'>Попробовать ещё раз</a>
        """)
    return HTMLResponse(f"""
    <h2>Информация о пользователе {info.username}:</h2>
    <p>Заработная плата: {zp_raise.wage} рублей</p>
    <p>Дата повышения: {zp_raise.raising}</p>
    <a href='/'>На главную</a>
    """)

