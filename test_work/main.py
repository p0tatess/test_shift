import uvicorn
from fastapi import Depends, FastAPI,Form
from sqlalchemy.orm import Session
from typing import Annotated
from . import crud
from .database import SessionLocal
from fastapi.responses import FileResponse,HTMLResponse


app = FastAPI()


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


# if __name__ == "__main__":
#     uvicorn.run(app, host="0.0.0.0", port=8000)

#Главная страница с пустой формой для регистрации
@app.get('/')
def index():
    return FileResponse('public/index.html')


#Создание или проверка пользователя
@app.post('/')
def create_user(
        username:Annotated[str,Form()],email:Annotated[str,Form()],password:Annotated[str,Form()],
         db: Session = Depends(get_db)
):
    db_user = crud.get_user_by_email(db, email=email)
    if db_user:
        return HTMLResponse("""
        <h3>Пользователь с такой почтой уже существует</h3>
        <a href='/'>Попробовать ещё раз</a>
        """)
    return crud.create_user(db=db, username=username,email=email,password=password)


#Страница с формой получения токена по логину и паролю
@app.get('/get_token')
def form_token():
    return FileResponse('public/get_info.html')


#Получение или обновление токена
@app.post('/get_token')
def get_own_token(username:Annotated[str,Form()],password:Annotated[str,Form()],db:Session=Depends(get_db)):
    return crud.get_secret_token(db=db,username=username,password=password)


#Страница с формой для получения информации по токену
@app.get('/info_token')
def get_form_info():
    return FileResponse('public/info_token.html')

#Получение информации о зарплате и дате повышения по токену
@app.post('/info_token')
def get_info_token(token:Annotated[str,Form()],db:Session=Depends(get_db)):
    return crud.token_info(token=token,db=db)


